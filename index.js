import createBoard from "./API/createBoard.js";
import displayBoards from "./API/displayBoards.js";


// display boards.
const getBoards = () => {
  displayBoards().then((data) => {
    data.forEach((board) => {
      let newBoard = document.createElement("div");
      newBoard.className = `newBoard w-60 h-28 bg-blue-800 p-4 m-4 rounded cursor-pointer`;
      newBoard.innerHTML = '<h5 class="text-white text-lg">' + board.name + "</h5>";
      newBoard.addEventListener("click", function(){
        window.location.href = './boards.html?id=' +board.id;
      });
      let boardList = document.getElementById("board-list");
      let button = document.getElementById("create-new-board");
      boardList.insertBefore(newBoard, button);
    });
  });
};
document.addEventListener('DOMContentLoaded', getBoards); 


  document.getElementById("input-form").addEventListener("submit", (e) => {
    const boardName = document.getElementById("board-name-input").value.trim();
    if (boardName != "") {
      createBoard(boardName);
      setTimeout(()=>{
        location.reload();
      },500)
    } 
  });
  document.getElementById("board-name-input").focus();


  document.getElementById('Trello').addEventListener('click', () => {
    location.reload();
  });