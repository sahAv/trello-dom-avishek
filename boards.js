import getLists from "./API/getLists.js";
import createList from "./API/createList.js";
import delList from "./API/deleteList.js";
import displayCards from "./API/displayCards.js";
import createCard from "./API/createCard.js";
import delCard from "./API/delCard.js";
import boardInfo from "./API/displayBoards.js";

// Function to display lists
const displayLists = async (boardId) => {
  try {
    const lists = await getLists(boardId);
    lists.forEach((list) => {
      const newListElement = createListElement(list);
      const listDiv = document.getElementById("list");
      const button = document.getElementById('add-new-list');
      listDiv.insertBefore(newListElement, button);
    });
  } catch (error) {
    console.error("Error fetching lists:", error);
  }
};

// Create lists.
    const boardId = new URLSearchParams(window.location.search).get("id");
    document.getElementById("input-form").addEventListener("submit", async (e) => {
      e.preventDefault();
      const listName = document.getElementById("list-name-input").value.trim();
      if (listName) {
        await createList(boardId, listName);
        window.location.reload();
      }
    })
 
    
// Function to create list element
const createListElement = (list) => {
  const newList = document.createElement("div");
  newList.className = "newList flex flex-col h-fit w-60 bg-indigo-50 p-4 m-4 rounded-xl cursor-pointer relative";

  const listName = document.createElement("h5");
  listName.id = list.id;
  listName.className = "text-dark text-base mb-2";
  listName.textContent = list.name;
  newList.appendChild(listName);

  // Add card button
  const addCardButton = createAddCardButton(list.id);
  newList.appendChild(addCardButton);

  // Add new card input after list name
  const addNewCardInput = createAddNewCardInput(list.id);
  newList.appendChild(addNewCardInput);
  addNewCardInput.style.display = 'none';
  addCardButton.addEventListener('click', () => {
    addNewCardInput.style.display = 'block';
    addCardButton.style.display = 'none';
  });

  // Display cards
  displayCards(list.id).then((cardNames) => {
    cardNames.forEach((cardName) => {
      const cardNameElement = createCardElement(cardName);
      newList.insertBefore(cardNameElement, addCardButton);
    });
  });

  // Delete list
  const closeButton = createCloseButton(() => {
    newList.remove();
    delList(list.id);
  });
  newList.appendChild(closeButton);

  return newList;
};

// Function to create add new card input
const createAddNewCardInput = (listId) => {
  const addNewCardInput = document.createElement("div");
  addNewCardInput.className = "add-new-card-input w-52 mt-1";

  const cardInput = document.createElement('input');
  cardInput.type = "text";
  cardInput.placeholder = "Enter title for card";
  cardInput.className = "card-input rounded-xl w-full text-sm p-1 pb-5";

  const submitCardButton = document.createElement("button");
  submitCardButton.textContent = "Add card";
  submitCardButton.className = "add-card-button rounded text-white bg-blue-500 m-2 p-1";
  submitCardButton.addEventListener('click', async () => {
    const cardName = cardInput.value.trim();
    if (cardName) {
      await createCard(listId, cardName);
      window.location.reload();
    }
  });
      // Add close card button. 
    const closeButton = document.createElement("button");
    closeButton.textContent = "X";
    closeButton.className = "close-button";
    closeButton.addEventListener('click', ()=>{
      addNewCardInput.style.display = 'none';

      const addCardButton = document.querySelector(`.add-card-button[data-list-id="${listId}"]`);
      addCardButton.style.display = 'block';
    });

  addNewCardInput.appendChild(cardInput);
  addNewCardInput.appendChild(submitCardButton);
  addNewCardInput.appendChild(closeButton);

  return addNewCardInput;
};

// Function to create card element
  const createCardElement = (cardName) => {
  const cardNameElement = document.createElement("div");
  cardNameElement.className = "flex text-dark text-sm rounded-xl bg-white my-1 p-2 w-full border-2 border-transparent hover:border-blue-500 relative";
  cardNameElement.textContent = cardName.name;

  // Delete card
  const closeButton = createCloseButton(() => {
    cardNameElement.remove();
    delCard(cardName.id);
  });
  closeButton.className = 'absolute top-1.5 right-1';
  closeButton.style.display = 'none';
  
  // Show close button on hover
  cardNameElement.addEventListener('mouseenter', () => {
    closeButton.style.display = 'block';
  });
  
  // Hide close button when not hovering
  cardNameElement.addEventListener('mouseleave', () => {
    closeButton.style.display = 'none';
  });
  cardNameElement.appendChild(closeButton);

  return cardNameElement;
};

// Function to create add card button
const createAddCardButton = (listId) => {
  const addCardButton = document.createElement("button");
  addCardButton.className = "add-card-button text-left text-sm text-dark rounded-xl w-52 p-2 mt-1 hover:bg-gray-300";
  addCardButton.textContent = "+ Add a card";
  addCardButton.setAttribute("data-list-id", listId);
  return addCardButton;
};

// Event listener for going back to home page
document.getElementById('Trello').addEventListener('click', () => {
  location.href = "./index.html";
});

// function call to display lists
document.addEventListener("DOMContentLoaded", () => {
  const boardId = new URLSearchParams(window.location.search).get("id");
  displayLists(boardId);
  boardInfo()
  .then((data)=>{
    data.forEach((board)=>{
      if(board.id === boardId){
        const boardName = document.createElement('div');
        boardName.textContent = board.name;
        boardName.className = "flex justify-center p-2 border-2";
        document.getElementById("boardName").appendChild(boardName);
      }
    })
  })
});


// Delete button.
const createCloseButton = (onClick) => {
  const closeButton = document.createElement("button");
  closeButton.className = "close-button text-dark hover:bg-gray-300 rounded-lg absolute top-3 right-4 p-1";

  // Create SVG element
  const svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
  svg.setAttribute("class", "h-6 w-6 text-gray-700");
  svg.setAttribute("width", "24");
  svg.setAttribute("height", "24");
  svg.setAttribute("viewBox", "0 0 24 24");
  svg.setAttribute("stroke-width", "2");
  svg.setAttribute("stroke", "currentColor");
  svg.setAttribute("fill", "none");
  svg.setAttribute("stroke-linecap", "round");
  svg.setAttribute("stroke-linejoin", "round");
  svg.innerHTML = `
    <path stroke="none" d="M0 0h24v24H0z"/>
    <line x1="4" y1="7" x2="20" y2="7" />
    <line x1="10" y1="11" x2="10" y2="17" />
    <line x1="14" y1="11" x2="14" y2="17" />
    <path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12" />
    <path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3" />
  `;
  closeButton.appendChild(svg);
  closeButton.addEventListener("click", onClick);
  return closeButton;
};