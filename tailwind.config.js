/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./*.{html,js}'],
  theme: {
    extend: {
      colors: {
        primaryColor: "#008de2"
      }
    },
  },
  plugins: [],
}

